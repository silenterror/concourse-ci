# Local Concourse

## Description

The purpose of this repo is to allow local rapid development using concourse ci.

## Prerequisites
docker-compose - [installation instructions](https://docs.docker.com/compose/install/)

### Getting Started

1) Clone this repository

```
	git clone git@gitlab.com:silenterror/concourse-ci.git
```


2) Change directory to concourse-local and run the compose


```
	cd concourse-local && docker-compose up
```
Place a -d at the end to run without viewing the input.  If you do that just remember to docker kill the containers they will stay up otherwise.


3) Go to [localhost](http://localhost:8080/) and login with **user:** test **password:** test


### Execute Tasks

For rapid development you can execute the tasks that the pipeline envokes.  First go to the task in the pipeline job and take note of any parameters that it needs.  You can pass those via --vars=USERNAME=some-user.  You can also create a yml file with the variables as well.

**Execute the Task File** 

Notice the input named service-foo= that I am pointing to locally so it can be uploaded to concourse since this is outside of the pipeline. Normally the pipeline clones it down in concourse but the tasks have no such ability when executed. So therefore you must point the execute to it.

```
fly execute -t ci.local -c build/ci/tasks/make-config.yml \
-i service-foo=/home/youruser/go/src/github.comcast.com/pps-portal/service-foo \
--load-vars-from=/home/youruser/etc/credentials/service-ci-make-config.yml
```

